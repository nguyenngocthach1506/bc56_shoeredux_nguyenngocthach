import React, { Component } from "react";
import ProductItem from "../ProductItem/ProductItem";
import { connect } from "react-redux";

class ProductList extends Component {
  renderProductItem = (list = []) => {
    if (list.length === 0) return;
    return list.map((item, index) => (
      <ProductItem key={"product" + index} product={item}></ProductItem>
    ));
  };
  render() {
    return (
      <div className="col-6">
        <div className="row row-gap-3 row-cols-2 row-cols-lg-3 bg-light p-2">
          {this.renderProductItem(this.props.products)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.shoe.products,
  };
};

export default connect(mapStateToProps)(ProductList);
