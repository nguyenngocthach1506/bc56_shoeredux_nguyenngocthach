import React, { Component } from "react";
import styles from "./Cart.module.css";
import base from "../../ulti/base";

import { Modal } from "antd";
import { connect } from "react-redux";
import Type from "../ShoesStore/constaint/constaint";

class Cart extends Component {
  renderCartList = (productList = []) => {
    if (productList.lenght === 0) return;
    return productList.map((product, index) => {
      return (
        <div className={styles.cartItem} key={"cartItem" + index}>
          <div>
            <img className={styles.cartImage} src={product.image} alt="" />
          </div>
          <p>{base.changeText(product.name, 5)}</p>
          <p>
            <a
              className={styles.btnInscrease}
              href="."
              onClick={(e) => {
                e.preventDefault();
                this.props.handleInscrease(+1, product.id);
              }}
            >
              <i className="bi bi-arrow-up-short"></i>
            </a>
            {/* number */}
            {product.number}
            <a
              className={styles.btnInscrease}
              href="."
              onClick={(e) => {
                e.preventDefault();
                this.props.handleInscrease(-1, product.id);
              }}
            >
              <i className="bi bi-arrow-down-short"></i>
            </a>
          </p>
          <p>{product.total.toLocaleString() + "$"}</p>
          <p>
            <button
              onClick={() => {
                this.props.handleRemove(product.id);
              }}
              className={styles.btnRemove}
              type="button"
            >
              <i className="bi bi-trash3-fill"></i>
            </button>
          </p>
        </div>
      );
    });
  };

  render() {
    const { cart: listCart } = this.props;

    const calAmount = (productList = []) => {
      let amount = productList.reduce(
        (result, value) => (result += value.total),
        0
      );
      return amount;
    };

    return (
      <div className="col-3">
        <div className={styles.cart + " bg-light"}>
          <div className={styles.table}>
            <div className={styles.heading}>
              <p>Image</p>
              <p>Name</p>
              <p>Quanity</p>
              <p>Total</p>
              <p>
                <i className="bi bi-gear-fill"></i>
              </p>
            </div>
            <div className={styles.tbody}>{this.renderCartList(listCart)}</div>
          </div>

          <div className="cart-footer w-100">
            <div className="table">
              <div className="row">
                <div className="col">
                  <p>SubTotal:</p>
                </div>
                <div className="col">
                  <p className="text-end">
                    {calAmount(listCart).toLocaleString() + "$"}
                  </p>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <p>Total:</p>
                </div>
                <div className="col">
                  <p className="text-end">
                    {calAmount(listCart).toLocaleString() + "$"}
                  </p>
                </div>
              </div>
            </div>
            <button
              onClick={() => {
                this.props.handleCheckOut();
                base.postLocalStorage([]);
                Modal.success({
                  title: "Succesfull !",
                  content: "Checkout is done.",
                });
              }}
              className={styles.btnCheckOut}
            >
              Check Out
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.shoe.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleInscrease: (option, id) => {
      let action = {
        type: Type.inscrease,
        payload: { option, id },
      };
      dispatch(action);
    },
    handleRemove: (id) => {
      let action = {
        type: Type.remove,
        payload: id,
      };
      dispatch(action);
    },
    handleCheckOut: () => {
      let action = {
        type: Type.checkout,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
