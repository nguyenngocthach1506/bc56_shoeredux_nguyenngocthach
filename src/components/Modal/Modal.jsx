import React, { Component } from "react";
import styles from "./Modal.module.css";

import { connect } from "react-redux";

class Modal extends Component {
  render() {
    const { productDetail: detail } = this.props;
    return (
      <div className={styles.modal + " col-3"}>
        <div className="detail-top">
          <img
            className="w-100 d-block mb-2"
            src={detail.image}
            alt={detail.image}
          />
        </div>
        <div className="detail-content">
          <h3 className="detail text-center mb-4">{detail.name}</h3>
          <p>
            <i className={styles.bi + " bi bi-box-seam"}></i>
            {detail.alias}
          </p>
          <p>
            <i className={styles.bi + " bi bi-cash-coin"}></i>
            {detail.price.toLocaleString() + "$"}
          </p>
          <p>
            <i className={styles.bi + " bi bi-info-square"}></i>
            {detail.description}
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    productDetail: state.shoe.productDetail,
  };
};

export default connect(mapStateToProps)(Modal);
