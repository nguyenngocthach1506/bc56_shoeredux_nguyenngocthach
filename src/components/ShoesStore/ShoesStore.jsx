import React, { Component } from "react";
import ProductList from "../ProductList/ProductList";
import Modal from "../Modal/Modal";
import Cart from "../Cart/Cart";

export default class ShoesStore extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <Cart></Cart>
          <ProductList></ProductList>
          <Modal></Modal>
        </div>
      </div>
    );
  }
}
