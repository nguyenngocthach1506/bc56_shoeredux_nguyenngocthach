import data from "../../../data.json";
import Type from "../constaint/constaint";
import base from "../../../ulti/base";
import { message } from "antd";

const initialState = {
    products: data,
    cart: (base.getLocalStorage() === null) ? [] : base.getLocalStorage(),
    productDetail: data[0],
}

const storeReducer = (state = initialState, { type, payload }) => {

    switch (type) {
        case (Type.add): return { ...state, cart: handleAddToCart(state.cart, payload) };
        case (Type.detail): return { ...state, productDetail: payload };
        case (Type.inscrease): return { ...state, cart: handleInscrease(state.cart, payload.option, payload.id) };
        case (Type.remove): return { ...state, cart: handleRemove(state.cart, payload) };
        case (Type.checkout): return { ...state, cart: [] };
        default:
            return state;
    }
}


const handleAddToCart = (cart, product) => {
    let cloneCart = [...cart];
    const index = base.searchFromList(cloneCart, product.id);
    if (index === -1) {
        let newProduct = { ...product, number: 1, total: product.price };
        cloneCart.push(newProduct);
    } else {
        return handleInscrease(cart, +1, cloneCart[index].id);
    }

    base.postLocalStorage(cloneCart)
    return cloneCart;
}


const handleInscrease = (cart, option = 0, id) => {
    let cloneCart = [...cart];
    let index = base.searchFromList(cloneCart, id);

    if (cloneCart.length === 0 && index === -1 && id === -1) return;

    // Tăng / Giảm số sản phẩm trong cart
    if (cloneCart[index].number >= 1) {
        cloneCart[index].number += option;
        cloneCart[index].total = cloneCart[index].price * cloneCart[index].number;
    }
    // Xóa shoe trong cart khi số sản phẩm == 0
    if (cloneCart[index].number <= 0) cloneCart = handleRemove(cart, id);
    base.postLocalStorage(cloneCart)
    return cloneCart;
}

const handleRemove = (cart, id = -1) => {
    let cloneCart = [...cart];
    let index = base.searchFromList(cloneCart, id);
    if (cloneCart.length === 0 && index === -1 && id === -1) return;

    index !== -1 &&
        cloneCart.splice(index, 1) &&
        message.open({ type: "success", content: "Xóa thành công" });
    base.postLocalStorage(cloneCart)
    return cloneCart;
};


export default storeReducer;