

const Type = {
    add: "shoe/add",
    detail: "shoe/detail",
    inscrease: "shoe/inscrease",
    remove: "shoe/deleteShoeFromCart",
    checkout: "shoe/checkOutShoe",
}


export default Type;