function changeText(text = "", number) {
    text = text.length <= number ? text : text.slice(0, number) + "...";
    return text;
};

function searchFromList(list = [], id) {
    let index = list.findIndex((item) => item.id === id)
    return index;
}

function postToLocalStorage(data, nameJson = 'cart',) {
    const string = JSON.stringify(data);
    localStorage.setItem(nameJson, string);
}

function getFromLocalStorage() {
    if (localStorage.getItem('cart')) {
        let valueJson = localStorage.getItem('cart');
        return JSON.parse(valueJson) || null;
    }
    else
        console.error("Error getItem from LocalStorage");

}

const base = {
    changeText: changeText,
    searchFromList: searchFromList,
    postLocalStorage: postToLocalStorage,
    getLocalStorage: getFromLocalStorage,
}

export default base;